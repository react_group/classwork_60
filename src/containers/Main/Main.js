import React, {useEffect, useState} from 'react';
import Messages from "../../components/Messages/Messages";



const url = 'http://146.185.154.90:8000/messages'

const Main = () => {

    const [messages, setMessages] = useState(
        []
    )


    useEffect(()=> {

        if (messages) {
            fetch(url)
                .then(response => response.json())
                .then(data => setMessages(data))
        };

        setInterval(() => {
            fetch(url)
                .then(response => response.json())
                .then(data => setMessages(data))
        }, 4000);
    }, []);

    return (
        <div className="main-block">
            <div className="container">
                <Messages
                    messages = {messages}
                    />
            </div>
        </div>
    );
};

export default Main;