import React from 'react';
import './Message.css'

const Message = ({message}) => {

    const data = message.datetime.substr(0, message.datetime.length -8).split('T').join(' ');

    return (
        <div className="card">
                <div className="card-body">
                        <h5 className="card-title">Author: {message.author}</h5>
                    <p className="card-text">Message: {message.message}</p>
                    <p className="card-text card-data">{data}</p>
                </div>
        </div>
    );
};

export default Message;