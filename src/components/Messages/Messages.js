import React from 'react';
import Message from "../Message/Message";

const Messages = ({messages}) => {
    return (
        <div className="message-list">
            {messages.map(m => (
                <Message
                    key = {m.datetime}
                    message = {m}
                />
            ))}
        </div>
    );
};

export default Messages;